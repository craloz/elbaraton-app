# ElbaratonApp

Proyecto en Angular para prueba frontend

#Detalles del Proyecto
El proyecto se genero usando:
    AngularCli Version 6.0.8
    Node Version 10.7.0
    Npm Version 6.1.0
#Instalar el proyecto
*Para instalar el proyecto es necesario las dependencias antes mencionadas

1) Pararse en la ruta del proyecto donde se encuentra el archivo package.json
2) Ejecutar el comando:
    $npm install para instalar todas las depedencias

#Ejecutar el proyecto
Para ejecutar el proyecto se utilizada el comando:
    $ng serve
Para ejecutar el proyecto abriendo el navegar utilizar el comando:
    $ng serve -o

#Detalles Relevantes

-Para poder buscar productos por categorias es necesario seleccionar cada una hasta que se marque en azul la casilla

-Si la categoria es un subnivel final se mostrara una caja de texto donde el usuario podra introducir el nombre del producto a buscar

-Para agregar o editar productos en el carrito de compra es necesario darle click al icono de carrito de compra al lado de la columna de available

-Para quitar productos en el carrito de compra es necesario darle click al icono de menos al lado del icono de carrito de compra