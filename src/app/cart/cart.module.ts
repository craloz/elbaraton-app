import { NgModule } from "@angular/core";
import { CartSummaryComponent } from "./cart-summary/cart-summary.component";
import { CartRoutingModule } from "./cart-routing.module";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { FormsModule } from "@angular/forms";

@NgModule({
    declarations: [
        CartSummaryComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        CartRoutingModule,
        AngularFontAwesomeModule
        
    ],
    providers: [
        
    ]
}) 

export class CartModule{}