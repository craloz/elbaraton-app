import { Component } from "@angular/core";
import { ShoppingCartService } from "src/app/shared/shopping-cart.service";
import { Product } from "src/app/shared/product.model";
import { Router } from "@angular/router";

@Component({
    selector: 'cart-summary',
    templateUrl: './cart-summary.component.html',
    styleUrls: ['./cart-summary.component.css']
})

export class CartSummaryComponent{

    constructor(private shoppingCartService: ShoppingCartService,
        private router: Router){
        
    }

    removeProduct(product: Product){
        this.shoppingCartService.removeProductFromCart(product)
    }

    checkout(){
        alert('Purchase Successfully Completed')
        this.shoppingCartService.checkout() 
        this.router.navigate(['dashboard']);
    }

}