import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CartSummaryComponent } from "./cart-summary/cart-summary.component";
const cartRoutes: Routes = [

    { path: '', component: CartSummaryComponent}

];

@NgModule({
    imports: [
        RouterModule.forChild(cartRoutes)
    ],
    exports: [RouterModule]
})
export class CartRoutingModule{}