import { Component, OnInit, Input } from "@angular/core";
import { Category } from "src/app/shared/category.model";
import { CategoryService } from "src/app/shared/category.service";
import { ProductService } from "src/app/shared/product.service";

@Component({
    selector: 'category-table',
    templateUrl: 'category-table.component.html',
    styleUrls: ['./category-table.component.css']
})

export class CategoryTableComponent implements OnInit{

    @Input('category') category: Category
    canShowChild: boolean = false;
    searchString: string = ''
    
    constructor(private categoryService: CategoryService,
        private productService: ProductService){

    }

    ngOnInit(){
    
    }

    onCategorySelected(){
        

        if(this.categoryService.selectedCategory === this.category){
            this.categoryService.selectedCategory = undefined
            this.canShowChild = false
        } else{
            this.categoryService.selectedCategory = this.category
            this.canShowChild = true
        }
    }

    isSelected(){
        return this.category === this.categoryService.selectedCategory ? true : false;
    }

    onSearch(){
        this.productService.onSearchByName(this.searchString);
    }
    

}