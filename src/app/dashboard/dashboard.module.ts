import { NgModule } from "@angular/core";
import { DashBoardRoutingModule } from "./dashboard-routing.module";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { SharedModule } from "../shared/shared.module";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { DashBoardComponent } from "./dashboard.component";
import { CategoryTableComponent } from "./category-table/category-table.component";
import { ProductTableComponent } from "./product-table/product-table.component";
import { ProductFiltersComponent } from "./product-table/product-filters/product-filters.component";
import { ProductAddCartModalComponent } from "./product-table/product-add-cart-modal/product-add-cart-modal.component";

@NgModule({
    declarations: [
        DashBoardComponent,
        CategoryTableComponent,
        ProductTableComponent,
        ProductFiltersComponent,
        ProductAddCartModalComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        DashBoardRoutingModule,
        AngularFontAwesomeModule
    ],
    providers: [
    ]
}) 
export class DashBoardModule{}