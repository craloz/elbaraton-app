import { Component, OnInit } from "@angular/core";
import { Category } from "../shared/category.model";
import { CategoryService } from "../shared/category.service";

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})

export class DashBoardComponent implements OnInit{
 
    categories: Array<Category>
    

    constructor(private categoryService: CategoryService){
        
    }

    ngOnInit(){

        this.categories = this.categoryService.getCategories()

    }

}