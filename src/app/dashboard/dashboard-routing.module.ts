import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashBoardComponent } from "./dashboard.component";
const dashBoardRoutes: Routes = [

    { path: '', component: DashBoardComponent}

];

@NgModule({
    imports: [
        RouterModule.forChild(dashBoardRoutes)
    ],
    exports: [RouterModule]
})
export class DashBoardRoutingModule{}