import { Component, Input, ViewChild } from "@angular/core";
import { Product } from "src/app/shared/product.model";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ShoppingCartService } from "src/app/shared/shopping-cart.service";
import { NgForm } from "@angular/forms";

@Component({
    selector: 'product-add-cart-modal',
    templateUrl: './product-add-cart-modal.component.html',
    styleUrls: ['./product-add-cart-modal.component.css']
})

export class ProductAddCartModalComponent{

    @ViewChild('content') content: any
    product: Product;
    isProductEditable: boolean = false;

    constructor(private modalService: NgbModal,
        private shoppingCartService: ShoppingCartService){
    }

    open(product: Product) {
        this.product = new Product(product.quantity, product.price, product.available,
            product.sublevel_id, product.name, product.id);
        this.isProductEditable = this.shoppingCartService.isProductInCart(this.product);
        if(this.isProductEditable){
            this.product.quantity = this.shoppingCartService.
                getShoppingCart()[this.shoppingCartService.indexOfProductInCart(this.product)].quantity
        } else{
            this.product.quantity = 0
        }
        this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title'});
    }

    addProductToShoppingCart(addCartForm: NgForm){
        this.isProductEditable = true
        this.product.quantity = addCartForm.value.quantity;
        this.shoppingCartService.addToCart(this.product)
        this.modalService.dismissAll();
    }
    

}