import { Component, OnInit, ViewChild } from "@angular/core";
import { ProductService } from "src/app/shared/product.service";
import { Product } from "src/app/shared/product.model";
import { CategoryService } from "src/app/shared/category.service";
import { ShoppingCartService } from "src/app/shared/shopping-cart.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ProductAddCartModalComponent } from "./product-add-cart-modal/product-add-cart-modal.component";

@Component({
    selector: 'product-table',
    templateUrl: './product-table.component.html',
    styleUrls: ['./product-table.component.css']
})

export class ProductTableComponent implements OnInit{

    @ViewChild('addProductModal') addProductModal: ProductAddCartModalComponent;

    products: Array<Product> = new Array()
    sortingField: string = '';
    sortingDirection: string = '';

    constructor(private productService: ProductService, 
        private categoryService: CategoryService,
        private shoppingCartService: ShoppingCartService,
        private modalService: NgbModal){

    }

    ngOnInit(){
        this.products = this.productService.getProducts()
    }

    isProductOnCategory(product: Product){
        if(this.categoryService.selectedCategory){
            return this.categoryService.selectedCategory.id === product.sublevel_id ? true : false 
        } else{
            return true;
        }
    }

    onSortProducts(sortingField: string){
        if(this.sortingField !== sortingField){
            this.sortingDirection = 'up'
        }else if(!this.sortingDirection){
            this.sortingDirection = 'up'
        } else{
            this.sortingDirection === 'up' ? this.sortingDirection = 'down' : this.sortingDirection ='up'
        }

        this.sortingField = sortingField;
  
        let sortedArray = this.products.sort((productA, productB) => {
            if (productA[sortingField] > productB[sortingField]) {
                return this.sortingDirection === 'up' ?  -1 : 1;
            }
        
            if (productA[sortingField] < productB[sortingField]) {
                return this.sortingDirection === 'up' ?  1 : -1;
            }
        
            return 0;
        })

        this.products = sortedArray;

    }

    addProductToCart(product: Product){
        this.addProductModal.open(product)
    }
    
    removeProduct(product: Product){
        this.shoppingCartService.removeProductFromCart(product)
    }

}