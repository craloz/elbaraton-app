import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ProductService } from "src/app/shared/product.service";

@Component({
    selector: 'product-filters',
    templateUrl: './product-filters.component.html',
    styleUrls: ['./product-filters.component.css']
})

export class ProductFiltersComponent{

    filterByPrinceRange: boolean = false

    filterByAvailability: boolean = false

    filterByQuantity: boolean = false;

    fromRangeFilterNumber: number

    toRangeFilterNumber: number

    availabilityFilterFlag: boolean

    quantityFilter: number

    constructor(private productService: ProductService){

    }

    doFilterByPriceRange(){
        this.filterByPrinceRange = !this.filterByPrinceRange
    }

    doFilterByAvailability(){
        this.filterByAvailability = !this.filterByAvailability
    }

    doFilterByQuantity(){
        this.filterByQuantity = !this.filterByQuantity       
    }

    applyRangeFilter(priceRangeForm: NgForm){
        
        this.fromRangeFilterNumber = priceRangeForm.value.from
        this.toRangeFilterNumber = priceRangeForm.value.to
        priceRangeForm.reset();
        this.productService.onSearchByPriceRange('$' + this.fromRangeFilterNumber.toString(), '$' + this.toRangeFilterNumber.toString())

    }

    removeRangeFilter(){
        this.fromRangeFilterNumber = undefined
        this.toRangeFilterNumber = undefined
        this.productService.onSearchByPriceRange('', '')
    }

    applyAvailabilityFilter(availabilityForm: NgForm){
        this.availabilityFilterFlag = <boolean>availabilityForm.value.availability
        availabilityForm.reset();
        this.productService.onSearchByAvailability(this.availabilityFilterFlag.toString())
    }

    removeAvailabilityFilter(){
        this.availabilityFilterFlag = undefined
        this.productService.onSearchByAvailability('')
    }

    applyQuantityFilter(quantityFilterForm: NgForm){
        
        this.quantityFilter = quantityFilterForm.value.quantity
        quantityFilterForm.reset()
        this.productService.onSearchByQuantity(this.quantityFilter.toString())
    }

    removeQuantityFilter(){
        this.quantityFilter = undefined
        this.productService.onSearchByQuantity('')
    }

}