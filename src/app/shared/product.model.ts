export class Product{

    quantity: number
    price: string
    available: boolean
    sublevel_id: number
    name: string
    id: string

    constructor(quantity: number, price: string, available: boolean,
        sublevel_id: number, name: string, id: string){

            this.quantity = quantity;
            this.price = price;
            this.available = available;
            this.sublevel_id = sublevel_id;
            this.name = name;
            this.id = id;
        
    }

}