import { Category } from "./category.model";
declare var require: any

export class CategoryService{
    
    private categories: Array<Category> = require('../categories.json').categories; 
    selectedCategory: Category;

    constructor(){
        
    }

    getCategories(){
        return this.categories.slice()
    }
}