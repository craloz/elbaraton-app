import { Product } from "./product.model";
import { Injectable } from "@angular/core";
import { StorageService } from "./storage.service";
import { ProductService } from "./product.service";

@Injectable()

export class ShoppingCartService{

    private shoppingCart: Array<Product> = new Array()

    constructor(private storageService: StorageService,
        private productsService: ProductService){
        this.loadShoppingCart();
    }

    loadShoppingCart(){
        this.shoppingCart = JSON.parse(this.storageService.getKey('shopping-cart'))
    }

    saveShoppingCart(){
        this.storageService.setKey('shopping-cart', JSON.stringify(this.shoppingCart))
    }

    getShoppingCart(){
        return this.shoppingCart.slice()
    }

    addToCart(product: Product){
        let productIndex = this.indexOfProductInCart(product)
        if( productIndex >= 0){
            this.shoppingCart[productIndex].quantity = product.quantity;
        } else{
            this.shoppingCart.push(product);
        }
        this.saveShoppingCart()
        
    }

    indexOfProductInCart(product: Product): number{
        for(const productInCart of this.shoppingCart){
            if(productInCart.id === product.id){
                return this.shoppingCart.indexOf(productInCart);
            }
        }
        return -1;
    }

    isProductInCart(product: Product){
        if (this.indexOfProductInCart(product) >= 0){
            return true
        } else {
            return false
        }
    }

    removeProductFromCart(product: Product){
        this.shoppingCart.splice(this.indexOfProductInCart(product), 1);
        this.saveShoppingCart()
    }

    getProductsCount(){
        return this.shoppingCart.length;
    }

    getProductsTotal(){
        let counter: number = 0
        for (const product of this.shoppingCart) {
            counter += (product.quantity * +product.price.substr(1).replace(',', ''))
        }
        return counter
    }

    checkout(){
        for(const product of this.shoppingCart){
            this.productsService.addProductQuantity(product);
        }
        this.shoppingCart = new Array()
        this.saveShoppingCart()
    }

}