import { NgModule } from "@angular/core";
import { FilterPipe } from "./filter.pipe";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { HeaderComponent } from "./header/header.component";
import { DropdownDirective } from "./header/dropdown.directive";

@NgModule({
    imports: [ 
        CommonModule,
        RouterModule
    ],
    declarations: [
        FilterPipe,
        HeaderComponent,
        DropdownDirective
    ],
    exports: [
        CommonModule,
        FilterPipe,
        HeaderComponent,
        DropdownDirective
    ]
})

export class SharedModule{}