import { Component } from "@angular/core";
import { ShoppingCartService } from "../shopping-cart.service";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})

export class HeaderComponent{

    isCartDropdownOpen = false;

    collapse: boolean = true;

    constructor(private shoppingCartService: ShoppingCartService){

    }

    onCartPressed(){
        this.isCartDropdownOpen = !this.isCartDropdownOpen
    }

    onCollapse(){
        this.collapse = !this.collapse
    }

}