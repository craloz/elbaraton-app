import { Directive, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[dropdown]'
})
export class DropdownDirective {
  @HostBinding('class.show') isOpen = false;

  @HostListener('click') toggleOpen() {
    
    this.isOpen = !this.isOpen;
  }
}
