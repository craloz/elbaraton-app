import { Product } from "./product.model";
import { Category } from "./category.model";
declare var require: any

export class ProductService{

    private products: Array<Product> = require('../products.json').products; 

    searchArray: Array<{filterValue: string, propertyName: string}> = new Array()

    nameFilter: {filterValue: string, propertyName: string} = {filterValue: '', propertyName: 'name'};
    availabilityFilter: {filterValue: string, propertyName: string} = {filterValue: '', propertyName: 'available'};
    quantityFilter: {filterValue: string, propertyName: string} = {filterValue: '', propertyName: 'quantity'};
    priceRangeFilter: {filterValue: string, propertyName: string, rightLimit: string} = {filterValue: '', propertyName: 'price', rightLimit: ''};

    constructor(){
        this.searchArray.push(this.nameFilter)
        this.searchArray.push(this.availabilityFilter)
        this.searchArray.push(this.quantityFilter)
        this.searchArray.push(this.priceRangeFilter)
    }

    getProducts(){
        return this.products.slice()
    }

    onSearchByName(filterValue: string){

        this.nameFilter.filterValue = filterValue
        this.searchArray[0] = this.nameFilter
    }

    onSearchByAvailability(filterValue: string){

        this.availabilityFilter = {
            filterValue: filterValue,
            propertyName: 'available'
        }
        this.searchArray[1] = this.availabilityFilter

    }

    onSearchByQuantity(filterValue: string){
        this.quantityFilter.filterValue = filterValue
        this.searchArray[2] = this.quantityFilter
    }

    onSearchByPriceRange(filterValue: string, rightLimit: string){
        this.priceRangeFilter.filterValue = filterValue;
        this.priceRangeFilter.rightLimit = rightLimit;
        this.searchArray[3] = this.priceRangeFilter;
    }

    addProductQuantity(productToAdd: Product){
        for(const product of this.products){
            if (product.id == productToAdd.id){
                product.quantity += +productToAdd.quantity
            }
        }
    }

    



}