export class StorageService{

    constructor(){

    }

    setKey(keyName: string, value: string){

        localStorage.setItem(keyName, value);

    }

    getKey(keyName: string): string{

        return localStorage.getItem(keyName)

    }


}