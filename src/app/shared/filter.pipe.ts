import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterArray: Array<{filterValue: string, propertyName: string, rightLimit: string}>): any {
    if (value.length === 0 || filterArray.length === 0) {
      return value;
    }
    const resultArray = [];
    for (const item of value) {
      let willEnter: boolean = true
      for (const filterItem of filterArray){      
        if(filterItem.rightLimit){
          if(filterItem.rightLimit && filterItem.filterValue){
            let leftLimitComparator = +filterItem.filterValue.substr(1);
            let rightLimitComparator = +filterItem.rightLimit.substr(1);
            let valueToCompare = +item[filterItem.propertyName].toString().substr(1).replace(',', '')
            if (!(valueToCompare >= leftLimitComparator  && 
              valueToCompare <= rightLimitComparator)) {
            
              willEnter = false;
    
            }
          } 
        } else{
          if (item[filterItem.propertyName].toString().indexOf(filterItem.filterValue) !== 0) {
          
            willEnter = false;
  
          }
        }
      }
      if(willEnter){
        resultArray.push(item)
      }
    }
    return resultArray;
  }

}