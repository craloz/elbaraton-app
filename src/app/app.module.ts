import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{ FormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing';
import { HomeComponent } from './home/home.component';
import { DashBoardComponent } from './dashboard/dashboard.component';
import { CategoryTableComponent } from './dashboard/category-table/category-table.component';
import { CategoryService } from './shared/category.service';
import { ProductService } from './shared/product.service';
import { ProductTableComponent } from './dashboard/product-table/product-table.component';
import { FilterPipe } from './shared/filter.pipe';
import { from } from 'rxjs';
import { ProductFiltersComponent } from './dashboard/product-table/product-filters/product-filters.component';
import { ShoppingCartService } from './shared/shopping-cart.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductAddCartModalComponent } from './dashboard/product-table/product-add-cart-modal/product-add-cart-modal.component';
import { StorageService } from './shared/storage.service';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFontAwesomeModule,
    NgbModule,
    SharedModule
  ],
  providers: [
    CategoryService, 
    ProductService,
    ShoppingCartService,
    StorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
